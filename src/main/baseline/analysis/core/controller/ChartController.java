package baseline.analysis.core.controller;

import baseline.analysis.core.service.EegProcessingService;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.WritableImage;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

public class ChartController {

	private final Logger LOG = LogManager.getLogger();

	@FXML
	LineChart<Number, Number> lineChart;

	private EegProcessingService service;


	@FXML
	public void showFileOpenChooser(ActionEvent event) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Resource File");
		fileChooser.getExtensionFilters().add(new ExtensionFilter("EPD Files", "*.epd"));
		File selectedFile = fileChooser.showOpenDialog(lineChart.getScene().getWindow());
		if (selectedFile != null) {
			startAsyncFFT(selectedFile);
		}
	}

	private void startAsyncFFT(File selectedFile) {
		Task<List<Series<Number, Number>>> task = createLoadDatasetTask(selectedFile);
		task.setOnSucceeded(workerStateEvent -> {
			try {
				LOG.debug("DATA in UI");
				lineChart.getData().addAll(task.get());
				LOG.debug("Done loading");
			} catch (InterruptedException | ExecutionException e) {
				LOG.error(e);
			}
		});
		Thread thread = new Thread(task);
		thread.setDaemon(true);
		thread.start();
	}

	private Task<List<Series<Number, Number>>> createLoadDatasetTask(File selectedFile) {
		return new Task<List<Series<Number, Number>>>() {

			@Override
			protected List<Series<Number, Number>> call() {
				return populateChart(selectedFile.getAbsolutePath());
			}
		};

	}

	private List<Series<Number, Number>> populateChart(String epdFilePath) {
		lineChart.getData().clear();
		service = new EegProcessingService(epdFilePath);

		service.readDataAndApplyFFT();

		double[][] trialPowers = service.getAlphaPowers();

		return createChartSeries(trialPowers);
	}

	private List<Series<Number, Number>> createChartSeries(double[][] trialPowers) {
		LOG.debug("Displaying Series");
		List<Series<Number, Number>> data = new ArrayList<>();
		for (int channel = 0; channel < 129; channel++) {
			XYChart.Series<Number, Number> series = new XYChart.Series<>();
			for (int trial = 0; trial < 210; trial++) {
				series.getData().add(new Data<>(trial, trialPowers[trial][channel]));
			}

			String channelName = "Ch-" + ((Integer) (channel + 1));
			series.setName(channelName);
			series.nodeProperty().addListener((observable, oldValue, newValue) -> {
				if (newValue != null) {
					newValue.setOnMouseClicked(event -> LOG.info(channelName));
				}
			});
			data.add(series);
		}
		LOG.debug("Created Series");
		return data;
	}

	private void takeSnapshot(File destination) {
		WritableImage snapshot = lineChart.snapshot(null, null);
		try {
			ImageIO.write(SwingFXUtils.fromFXImage(snapshot, null), "png", destination);
			LOG.debug("snapshot saved: " + destination.getAbsolutePath());
		} catch (IOException ex) {
			LOG.error(ex);
		}
	}

	@FXML
	public void takeSnapshot(ActionEvent event) {
		takeSnapshot(showSaveFileChooser("*.png"));
	}

	private File showSaveFileChooser(String extension) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().add(new ExtensionFilter(extension, extension));
		return fileChooser.showSaveDialog(lineChart.getScene().getWindow());
	}

	@FXML
	public void removeChannel(ActionEvent event) {
		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle("Select Channel Number");
		dialog.setContentText("Please enter the channel number:");
		Optional<String> result = dialog.showAndWait();
		result.ifPresent(number -> removeChannel(number));
	}

	private void removeChannel(String number) {
		if (StringUtils.isNumeric(number)) {
			long chNb = Long.parseLong(number);
			LOG.info("Removing channel-" + chNb);
			Iterator<Series<Number, Number>> iterator = lineChart.getData().iterator();
			while (iterator.hasNext()) {
				Series<Number, Number> series = iterator.next();
				if (series.getName().equals("Ch-" + chNb)) {
					iterator.remove();
					LOG.info("Removed channel-" + chNb);
				}
			}
		} else {
			LOG.error("Input is not a number");
		}
	}
}
