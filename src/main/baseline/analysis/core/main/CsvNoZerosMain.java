package baseline.analysis.core.main;

import baseline.analysis.core.service.writting.CsvTrimmer;

import java.io.File;


public class CsvNoZerosMain {

	private static File sourceFolder = new File("C:\\EEG\\Exports\\csv");
	private static File outputFolder = new File("C:\\EEG\\Exports\\csv\\filtered");

	public static void main(String[] args) throws Exception {
		outputFolder.mkdirs();
		for (File file : sourceFolder.listFiles()) {
			if (file.isFile()) { //i.e. Not a folder
				CsvTrimmer trimmer = new CsvTrimmer(file, outputFolder);
				trimmer.readData();
				trimmer.filterColumns();
				trimmer.removeUncertain();
				trimmer.filterColumnsAndRemoveUncertain();
			}
		}
	}
}
