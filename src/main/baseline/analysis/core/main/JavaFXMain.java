package baseline.analysis.core.main;

import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class JavaFXMain extends Application {

	@Override
	public void start(Stage stage) throws Exception {
		URL fxmlUrl = this.getClass().getClassLoader().getResource("Main.fxml");
		VBox root = FXMLLoader.load(fxmlUrl);
		stage.setTitle("Eeg Baseline Visualization");
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
