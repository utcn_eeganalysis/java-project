package baseline.analysis.core.main;

import baseline.analysis.core.service.EegProcessingService;
import baseline.analysis.core.service.writting.CsvWriter;
import baseline.analysis.core.util.EpdUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;


public class CsvConsoleMain {

	public static final String OUTPUT_FOLDER = "C:\\EEG\\Exports\\csv";
	public static final String SOURCE_FOLDER = "C:\\EEG\\Dots_30";
	private static      File   outputFolder  = new File(OUTPUT_FOLDER);

	public static void main(String[] args) throws Exception {
		List<File> epdFiles = EpdUtil.collectEpdFiles(new File(SOURCE_FOLDER));
		CsvWriter csvWriter = new CsvWriter(new File(outputFolder, "AllDots.csv"));
		CsvWriter csvWriterPhase = new CsvWriter(new File(outputFolder, "AllDots-phase.csv"));
		CsvWriter csvWriterZScore = new CsvWriter(new File(outputFolder, "AllDots-Z-Score.csv"));
		CsvWriter csvWriterIQR = new CsvWriter(new File(outputFolder, "AllDots-IQR.csv"));

		for (File epdFile : epdFiles) {
			EegProcessingService processor = new EegProcessingService(epdFile.getPath());
			processor.readDataAndApplyFFT();
			String datasetName = epdFile.getParentFile().getName();
			writeCsvData(csvWriter, csvWriterZScore, csvWriterIQR, csvWriterPhase, processor);
			writeCsvData(powers(datasetName), zScore(datasetName), iqr(datasetName), phase(datasetName), processor);
		}
		close(csvWriter, csvWriterIQR, csvWriterZScore);
	}

	private static CsvWriter phase(String datasetName) throws FileNotFoundException {
		return new CsvWriter(new File(outputFolder, datasetName + "-phase.csv"));
	}

	private static CsvWriter iqr(String datasetName) throws FileNotFoundException {
		return new CsvWriter(new File(outputFolder, datasetName + "-IQR.csv"));
	}

	private static CsvWriter powers(String datasetName) throws FileNotFoundException {
		return new CsvWriter(new File(outputFolder, datasetName + ".csv"));
	}

	private static CsvWriter zScore(String datasetName) throws FileNotFoundException {
		return new CsvWriter(new File(outputFolder, datasetName + "-Z-Score.csv"));
	}

	private static void writeCsvData(CsvWriter csvWriter, CsvWriter csvWriterZScore, CsvWriter csvWriterIQR, CsvWriter csvWriterPhase, EegProcessingService processor) {
		List<Integer> labels = processor.getLabels();
		csvWriter.writeData(processor.getConfiguredPowers(), labels);
		csvWriterZScore.writeData(processor.getConfiguredZScore(), labels);
		csvWriterIQR.writeData(processor.getConfiguredIqrScore(), labels);
		csvWriterPhase.writeData(processor.getConfiguredPhase(), labels);
	}

	private static void close(CsvWriter... csvWriters) {
		for (CsvWriter writer : csvWriters) {
			writer.close();
		}
	}
}
