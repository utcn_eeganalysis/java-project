package baseline.analysis.core.main;

import baseline.analysis.core.service.EegProcessingService;
import baseline.analysis.core.service.writting.ExcelWriter;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static baseline.analysis.core.util.EpdUtil.collectEpdFiles;


public class ExcelConsoleMain {

	public static final String OUTPUT_FOLDER = "C:\\EEG\\Exports-Baseline0";
	public static final String SOURCE_FOLDER = "C:\\EEG\\Dots_30";

	public static void main(String[] args) throws Exception {
		File outputFolder = new File(OUTPUT_FOLDER);
		if(!outputFolder.exists()){
			outputFolder.mkdirs();
		}
		List<File> epdFiles = getEpdFile(false);

		ExcelWriter writer = new ExcelWriter(new File(outputFolder, "AllDots.xlsx"));
		writer.writeHeaders();
		for (File epdFile : epdFiles) {
			EegProcessingService processor = new EegProcessingService(epdFile.getPath());
			processor.readDataAndApplyFFT();
			String datasetName = epdFile.getParentFile().getName();
			processor.exportToExcel(writer);
			processor.exportToExcel(new File(outputFolder, datasetName + ".xlsx"));
		}
		writer.writeFormulas();
		writer.flush();
	}

	private static List<File> getEpdFile(boolean test) {
		if (test) {
			return Arrays.asList(
					//new File("C:\\EEG\\Dots_30\\Dots_30_001\\Dots_30_001.epd"),
					new File("C:\\EEG\\Dots_30\\Dots_30_002\\Dots_30_002.epd"));
		}
		return collectEpdFiles(new File(SOURCE_FOLDER));
	}
}
