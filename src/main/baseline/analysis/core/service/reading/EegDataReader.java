package baseline.analysis.core.service.reading;

import baseline.analysis.core.service.model.ChannelBaselineData;
import baseline.analysis.core.service.model.EegDataset;
import baseline.analysis.core.service.model.Event;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import static baseline.analysis.core.util.ThreadUtil.waitForExecutionToFinish;

/**
 * Class responsible for reading data of an EEG Experiment. <br/>
 * This means reading the .epd (EEG Processor Dataset) File, the Event Codes,
 * the Event Timestamps and the 128 concurrent channels.
 *
 * @author Buzea Vlad-Calin (buzea.vlad@gmail.com)
 */
public class EegDataReader implements AutoCloseable {

	private final Logger         LOG = LogManager.getLogger();
	private       File           epdFile;
	private       BufferedReader reader;
	private       File           containingFolder;
	private final int            baselineStartCode;
	private final int            baselineEndCode;


	public EegDataReader(String epdFilePath, int baselineStartCode, int baselineEndCode) throws IOException {
		epdFile = new File(epdFilePath);
		containingFolder = epdFile.getParentFile();
		reader = new BufferedReader(new FileReader(epdFile));
		this.baselineStartCode = baselineStartCode;
		this.baselineEndCode = baselineEndCode;
	}

	public EegDataset readDataSet() throws IOException {
		EegDataset dataSet = new EegDataset();
		String fileName = epdFile.getName();
		String dataSetName = fileName.substring(0, fileName.lastIndexOf('.'));
		dataSet.setName(dataSetName);
		int numberOfChannels = readNumberOfChannels();
		dataSet.setSamplingFrequency(readSamplingFrequency());
		dataSet.setNumberOfSamples(readNumberOfSamples());
		dataSet.setEvents(readEventData(numberOfChannels));
		dataSet.setBaselineData(readBaselineData(numberOfChannels, dataSet));
		return dataSet;
	}

	private List<ChannelBaselineData> readBaselineData(int numberOfChannels, EegDataset dataSet) throws IOException {
		List<ChannelBaselineData> result = initResult(numberOfChannels);
		List<Thread> readingTasks = new ArrayList<>();
		List<Pair<Integer, Integer>> baselineLimits = calculateBaselineLimits(dataSet);
		for (int i = 1; i <= numberOfChannels; i++) {
			Thread createTask = createTask(dataSet.getName(), dataSet.getNumberOfSamples(), baselineLimits, result, i);
			readingTasks.add(createTask);
			createTask.start();
		}
		waitForExecutionToFinish(readingTasks);
		return result;
	}

	private List<ChannelBaselineData> initResult(int numberOfChannels) {
		List<ChannelBaselineData> result = new ArrayList<>(130);
		for (int i = 0; i <= numberOfChannels; i++) {
			result.add(new ChannelBaselineData(i));
		}
		return result;
	}

	private List<Pair<Integer, Integer>> calculateBaselineLimits(EegDataset dataSet) {
		List<Pair<Integer, Integer>> baselineLimits = new ArrayList<>(210);
		for (Iterator<Event> iterator = dataSet.getEvents().iterator(); iterator.hasNext(); ) {
			Event event1 = iterator.next();
			if (event1.getCode() == baselineStartCode) {
				Event event2 = iterator.next();
				if (event2.getCode() != baselineEndCode) {
					throw new RuntimeException("Corrupt Dataset!\n" +
							" Expected (" + baselineStartCode + ", " + baselineEndCode + ")\n" +
							"Found(" + event1.getCode() + ", " + event2.getCode() + ")");
				}
				baselineLimits.add(new ImmutablePair<>(event1.getTimestamp(), event2.getTimestamp()));
			}
		}
		return baselineLimits;
	}

	private Thread createTask(String dataSetName, int numberOfSamples, List<Pair<Integer, Integer>> baselineLimits,
							  List<ChannelBaselineData> result, int i) {
		return new Thread(() -> readBaselines(dataSetName, numberOfSamples, baselineLimits, result, i));
	}

	private void readBaselines(String dataSetName, int numberOfSamples, List<Pair<Integer, Integer>> baselineLimits,
							   List<ChannelBaselineData> result, int i) {

		String fileName = String.format("%s-Ch%03d.bin", dataSetName, i);
		LOG.debug("Reading " + fileName);
		try (EegChannelReader channelReader = new EegChannelReader(new File(containingFolder, fileName), numberOfSamples)) {
			ChannelBaselineData channelBaselineData = result.get(i);
			for (Pair<Integer, Integer> pair : baselineLimits) {
				Vector<Float> channelData = channelReader.readDataBetween(pair.getLeft(), pair.getRight());
				channelBaselineData.getBaselines().add(channelData);
			}
			LOG.debug("Finished reading " + fileName);
		} catch (Exception e) {
			LOG.error(e);
		}
	}

	private List<Event> readEventData(int numberOfChannels) throws IOException {
		readUselessLines(numberOfChannels + 4);
		String eventTimestampsFile = reader.readLine();
		readUselessLines(2);
		String eventCodesFile = reader.readLine();
		readUselessLines(2);
		int numberOfEvents = Integer.parseInt(reader.readLine());
		EegEventReader eventReader = new EegEventReader(new File(containingFolder, eventCodesFile),
				new File(containingFolder, eventTimestampsFile), numberOfEvents);
		List<Event> readEventData = eventReader.readEventData();
		return readEventData;
	}

	private int readNumberOfSamples() throws IOException {
		readUselessLines(2);
		return Integer.parseInt(reader.readLine());
	}

	private int readSamplingFrequency() throws IOException {
		readUselessLines(2);
		return (int) Float.parseFloat(reader.readLine());
	}

	private int readNumberOfChannels() throws IOException {
		readUselessLines(5);
		return Integer.parseInt(reader.readLine());
	}

	private void readUselessLines(int nbOfLines) throws IOException {
		for (int i = 0; i < nbOfLines; i++) {
			reader.readLine();
		}
	}

	@Override
	protected void finalize() throws Throwable {
		reader.close();
		super.finalize();
	}

	@Override
	public void close() throws Exception {
		reader.close();
	}
}
