package baseline.analysis.core.service.reading;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Vector;

import static baseline.analysis.core.util.ConversionUtil.littleEndianToBigEndian;

/**
 * Class responsible for reading the data of an EEG Channel from a binary file.
 *
 * @author Buzea Vlad-Calin (buzea.vlad@gmail.com)
 */
public class EegChannelReader implements AutoCloseable {

	private final Logger LOG	= LogManager.getLogger();
	private static final int	INTEGER_NUMBER_OF_BYTES	= 4;
	private RandomAccessFile	binaryFile;
	private int					numberOfSamples;

	/**
	 * Constructor.
	 * <i>Note:</i> The number of samples in the binary file is specified in the .epd file
	 *
	 * @param binaryFile
	 *            binary file
	 * @param numberOfSamples
	 *            the number of samples (values) in the binary file
	 * @throws FileNotFoundException
	 */
	public EegChannelReader(File binaryFile, int numberOfSamples) throws FileNotFoundException {
		this.numberOfSamples = numberOfSamples;
		this.binaryFile = new RandomAccessFile(binaryFile, "r");
	}

	/**
	 * Reads the data from the file and returns it in a {@link Vector}
	 *
	 * @return {@link Vector} with EEG voltage values
	 */
	public Vector<Float> readData() {
		return readDataBetween(0, numberOfSamples);
	}

	public Vector<Float> readDataBetween(int startIndex, int endIndex) {
		Vector<Float> data = new Vector<>(endIndex - startIndex);
		try {
			binaryFile.seek(INTEGER_NUMBER_OF_BYTES * startIndex);
			for (int i = startIndex; i < endIndex; i++) {
				int bits = binaryFile.readInt();
				float value = convertBitsToFloat(bits);
				data.add(value);
			}
		} catch (IOException e) {
			System.err.println(startIndex + " " + endIndex + " (" + numberOfSamples + ")");
			e.printStackTrace();
		}
		return data;

	}

	private float convertBitsToFloat(int bits) {
		// it may sound stupid to convert from little to big endian for floats,
		// but reading it works correctly this way
		int bigEndianBits = littleEndianToBigEndian(bits);
		return Float.intBitsToFloat(bigEndianBits);
	}

	@Override
	protected void finalize() throws Throwable {
		binaryFile.close();
		super.finalize();
	}

	@Override
	public void close() throws Exception {
		binaryFile.close();
	}
}
