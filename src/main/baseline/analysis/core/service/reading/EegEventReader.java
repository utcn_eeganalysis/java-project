package baseline.analysis.core.service.reading;

import baseline.analysis.core.service.model.Event;
import org.apache.commons.lang3.tuple.Pair;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static baseline.analysis.core.util.ConversionUtil.littleEndianToBigEndian;

/**
 * Class responsible for reading the pairs of (EventCode,EventTimestamp)
 *
 * @author Buzea Vlad-Calin (buzea.vlad@gmail.com)
 */
public class EegEventReader {

	private File	eventCodesFile;
	private File	eventTimestampsFile;
	private int		numberOfEvents;

	/**
	 * Constructor.
	 * <i>Note:</i> The Number of events in the binary file is specified in the .epd file
	 *
	 * @param eventCodesFile
	 *            event codes binary file
	 * @param eventTimestampsFile
	 *            event timestamps binary file
	 * @param numberOfEvents
	 *            the number of samples (values) in the binary file
	 */
	public EegEventReader(File eventCodesFile, File eventTimestampsFile, int numberOfEvents) {
		this.numberOfEvents = numberOfEvents;
		this.eventCodesFile = eventCodesFile;
		this.eventTimestampsFile = eventTimestampsFile;
	}

	/**
	 * Reads the data from the files and returns it in a {@link List} of {@link Pair}s
	 *
	 * @return {@link List} of EventCode-EventTimestamp pairs
	 * @throws IOException
	 *             if one of the binary files is not found or is broken
	 */
	public List<Event> readEventData() throws IOException {
		List<Event> data = new ArrayList<>(numberOfEvents);
		try (DataInputStream eventCodesInputStream = new DataInputStream(new FileInputStream(eventCodesFile));
				DataInputStream eventTimestampsStream = new DataInputStream(new FileInputStream(eventTimestampsFile))) {
			for (int i = 0; i < numberOfEvents; i++) {
				int eventCode = littleEndianToBigEndian(eventCodesInputStream.readInt());
				int eventTimestamp = littleEndianToBigEndian(eventTimestampsStream.readInt());
				data.add(new Event(eventCode, eventTimestamp));
			}
			return data;
		}
	}


}
