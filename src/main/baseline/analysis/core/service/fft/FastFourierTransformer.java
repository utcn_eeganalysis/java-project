package baseline.analysis.core.service.fft;

import baseline.analysis.core.service.model.ChannelBaselineData;
import baseline.analysis.core.service.model.EegDataset;
import org.apache.commons.math3.complex.Complex;
import org.jtransforms.fft.FloatFFT_1D;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import static baseline.analysis.core.util.ThreadUtil.waitForExecutionToFinish;
import static org.apache.commons.lang3.ArrayUtils.toPrimitive;

public class FastFourierTransformer {

	private final EegDataset             dataSet;
	private final FrequencyDomainDataset result;

	public FastFourierTransformer(EegDataset dataSet) {
		this.dataSet = dataSet;
		int baselineLength = dataSet.getBaselineData().get(1).getBaselines().get(1).size() / 2;
		this.result = new FrequencyDomainDataset(dataSet.getSamplingFrequency(), baselineLength);
	}

	public void applyTransformOnAllChannels() {
		List<Thread> tasks = new ArrayList<>();
		for (ChannelBaselineData channelBaselineData : dataSet.getBaselineData()) {
			Thread task = new Thread(() -> transformChannelData(channelBaselineData));
			task.start();
			tasks.add(task);
		}
		waitForExecutionToFinish(tasks);
	}



	private void transformChannelData(ChannelBaselineData channelBaselineData) {
		int channelNumber = channelBaselineData.getChannelNumber();
		FrequencyDomainChannelData ffChanelData = result.get(channelNumber);
		for (Vector<Float> baselineData : channelBaselineData.getBaselines()) {
			List<Complex> fourier = convertToFourier(baselineData);
			ffChanelData.add(fourier);
		}

	}

	private List<Complex> convertToFourier(Vector<Float> baselineData) {
		if (baselineData.size() % 2 != 0) {
			baselineData.remove(baselineData.size() - 1);
		}
		float[] fft = toPrimitive(baselineData.toArray(new Float[baselineData.size()]), 0.0F);
		FloatFFT_1D transformer = new FloatFFT_1D(fft.length);
		transformer.realForward(fft);
		List<Complex> fourier = convertToComplex(fft);
		return fourier;
	}

	// @formatter:off
	/*
	 * a[2*k] = Re[k], 0<=k<n/2 a[2*k+1] = Im[k], 0<k<n/2 a[1] = Re[n/2]
	 */
	// @formatter:on
	private static List<Complex> convertToComplex(float[] a) {
		int n = a.length;
		List<Complex> result = new ArrayList<>();
		result.add(new Complex(a[0]));
		for (int k = 1; k < n / 2; k++) {
			float real = a[2 * k];
			float imaginary = a[2 * k + 1];
			result.add(new Complex(real, imaginary));
		}
		result.add(new Complex(a[1]));
		return result;
	}

	public FrequencyDomainDataset getResult() {
		return result;
	}
}
