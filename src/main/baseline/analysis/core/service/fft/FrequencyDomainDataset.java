package baseline.analysis.core.service.fft;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static baseline.analysis.core.util.ComplexUtil.*;

public class FrequencyDomainDataset implements Iterable<FrequencyDomainChannelData> {


	private final List<FrequencyDomainChannelData> channelData;

	public FrequencyDomainDataset(int samplingFrequency, int baselineLength) {
		int alphaStartIndex = calculateAlphaStartIndex(samplingFrequency, baselineLength);
		int alphaEndIndex = calculateAlphaEndIndex(samplingFrequency, baselineLength);
		int gammaStartIndex = calculateGammaStartIndex(samplingFrequency, baselineLength);
		int gammaEndIndex = calculateGammaEndIndex(samplingFrequency, baselineLength);
		int configuredStartIndex = calculateConfiguredFrequencyStartIndex(samplingFrequency, baselineLength);
		int configuredEndIndex = calculateConfiguredFrequencyEndIndex(samplingFrequency, baselineLength);
		channelData = new ArrayList<>(129);
		for (int i = 0; i < 129; i++) {
			channelData.add(new FrequencyDomainChannelData(
					alphaStartIndex, alphaEndIndex,
					gammaStartIndex, gammaEndIndex,
					configuredStartIndex, configuredEndIndex));
		}
	}


	public FrequencyDomainChannelData get(int channelNumber) {
		return channelData.get(channelNumber);
	}

	@Override
	public Iterator<FrequencyDomainChannelData> iterator() {
		return channelData.iterator();
	}

}
