package baseline.analysis.core.service.fft;

import org.apache.commons.math3.complex.Complex;

import java.util.ArrayList;
import java.util.List;

public class FrequencyDomainChannelData {

	private final List<List<Complex>> baselines = new ArrayList<>();

	private int alphaStart;
	private int alphaEnd;

	private int gammaStart;
	private int gammaEnd;

	private int configuredStartIndex;
	private int configuredEndIndex;

	public FrequencyDomainChannelData(int alphaStart, int alphaEnd, int gammaStart, int gammaEnd, int configuredStartIndex, int configuredEndIndex) {
		this.alphaStart = alphaStart;
		this.alphaEnd = alphaEnd;
		this.gammaStart = gammaStart;
		this.gammaEnd = gammaEnd;
		this.configuredStartIndex = configuredStartIndex;
		this.configuredEndIndex = configuredEndIndex;
	}

	/**
	 * Adds data for a new baseline, from the same channel, different trial
	 *
	 * @param fftBaseline baseline data in frequency domain
	 * @return true if ok, false otherwise
	 */
	public boolean add(List<Complex> fftBaseline) {
		return baselines.add(fftBaseline);
	}


	public List<Complex> getAlphaBins(int trialNumber) {
		return baselines.get(trialNumber).subList(alphaStart, alphaEnd);
	}

	public List<Complex> getConfiguredPower(int trialNumber) {
		return baselines.get(trialNumber).subList(configuredStartIndex, configuredEndIndex);
	}


	public List<Complex> getGammaBins(int trialNumber) {
		return baselines.get(trialNumber).subList(gammaStart, gammaEnd);
	}
}
