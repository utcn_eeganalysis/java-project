package baseline.analysis.core.service.writting;

import baseline.analysis.core.util.EpdUtil;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.charts.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import static baseline.analysis.core.service.writting.ExcelWriter.DataSheet.*;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.poi.ss.util.CellReference.convertNumToColString;

public class ExcelWriter implements AutoCloseable {

	public enum DataSheet {
		ALPHA_POWERS("Alpha-Powers", true),
		ALPHA_PHASE("Alpha-Phase", true),
		GAMMA_POWERS("Gamma-Powers", true),
		ALPHA_ZSCORE("Alpha-Z-Scores", true),
		GAMMA_ZSCORE("Gamma-Z-Scores", true),
		ALPHA_IQR_SCORE("Alpha-IQR-Scores", true),
		GAMMA_IQR_SCORE("Gamma-IQR-Scores", true),
		STATISTICS("Statistics", false),
		FORMULAS("Formulas", false),
		SEEN_VS_UNSEEN("SeenVsUnseen", false);

		private final String sheetLabel;

		private final boolean isMatrix;

		public boolean isMatrix() {
			return isMatrix;
		}

		public String getSheetLabel() {
			return sheetLabel;
		}

		DataSheet(String label, boolean isMatrix) {
			sheetLabel = label;
			this.isMatrix = isMatrix;
		}

	}

	private final Logger   LOG = getLogger();
	private final Workbook workbook;

	private final File outputFile;
	private       int  chartOffset;

	public ExcelWriter(File outputFile) {
		this.outputFile = outputFile;
		workbook = new XSSFWorkbook();
		for (DataSheet sheet : DataSheet.values()) {
			workbook.createSheet(sheet.getSheetLabel());
		}
	}

	private Sheet getSheet(DataSheet dataSheet) {
		return workbook.getSheet(dataSheet.getSheetLabel());
	}

	public void writeData(DataSheet dataSheet, double[][] data, List<Integer> labels, String datasetName) {
		Sheet sheet = getSheet(dataSheet);
		for (int trialNumber = 0; trialNumber < 210; trialNumber++) {
			Row row = sheet.createRow(sheet.getLastRowNum() + 1);
			Cell cell = row.createCell(0);
			cell.setCellValue(datasetName + "-" + (trialNumber + 1));
			cell = row.createCell(1);
			cell.setCellValue(labels.get(trialNumber));
			for (int i = 0; i < 128; i++) {
				cell = row.createCell(i + 2);
				cell.setCellValue(data[trialNumber][i]);
			}
		}
	}

	public void writeStatistics(DescriptiveStatistics statistics) {
		Sheet sheet = getSheet(STATISTICS);
		Cell cell;
		int count = 0;
		Row row = sheet.createRow(count++);
		cell = row.createCell(0, CellType.STRING);
		cell.setCellValue("Mean");
		cell = row.createCell(1, CellType.NUMERIC);
		cell.setCellValue(statistics.getMean());

		row = sheet.createRow(count++);
		cell = row.createCell(0, CellType.STRING);
		cell.setCellValue("SD Dev");
		cell = row.createCell(1, CellType.NUMERIC);
		cell.setCellValue(statistics.getStandardDeviation());

		row = sheet.createRow(count++);
		cell = row.createCell(0, CellType.STRING);
		cell.setCellValue("Skewness");
		cell = row.createCell(1, CellType.NUMERIC);
		cell.setCellValue(statistics.getSkewness());

		row = sheet.createRow(count++);
		cell = row.createCell(0, CellType.STRING);
		cell.setCellValue("q1");
		cell = row.createCell(1, CellType.NUMERIC);
		cell.setCellValue(statistics.getPercentile(25));

		row = sheet.createRow(count++);
		cell = row.createCell(0, CellType.STRING);
		cell.setCellValue("q2");
		cell = row.createCell(1, CellType.NUMERIC);
		cell.setCellValue(statistics.getPercentile(50));

		row = sheet.createRow(count++);
		cell = row.createCell(0, CellType.STRING);
		cell.setCellValue("q3");
		cell = row.createCell(1, CellType.NUMERIC);
		cell.setCellValue(statistics.getPercentile(75));

		row = sheet.createRow(count++);
		cell = row.createCell(0, CellType.STRING);
		cell.setCellValue("Variance");
		cell = row.createCell(1, CellType.NUMERIC);
		cell.setCellValue(statistics.getVariance());

		row = sheet.createRow(count++);
		cell = row.createCell(0, CellType.STRING);
		cell.setCellValue("Population variance");
		cell = row.createCell(1, CellType.NUMERIC);
		cell.setCellValue(statistics.getPopulationVariance());

		row = sheet.createRow(count++);
		cell = row.createCell(0, CellType.STRING);
		cell.setCellValue("Geometrical Mean");
		cell = row.createCell(1, CellType.NUMERIC);
		cell.setCellValue(statistics.getGeometricMean());

		row = sheet.createRow(count++);
		cell = row.createCell(0, CellType.STRING);
		cell.setCellValue("Kurtosis");
		cell = row.createCell(1, CellType.NUMERIC);
		cell.setCellValue(statistics.getKurtosis());

		row = sheet.createRow(count++);
		cell = row.createCell(0, CellType.STRING);
		cell.setCellValue("Quadratic Mean");
		cell = row.createCell(1, CellType.NUMERIC);
		cell.setCellValue(statistics.getQuadraticMean());
	}

	public void writeFormulas() {
		LOG.debug("Writing Formulas");
		Sheet sheet = getSheet(FORMULAS);
		Row header = sheet.createRow(0);
		header.createCell(1).setCellValue("Alpha-Average");
		header.createCell(2).setCellValue("Alpha-Median");
		header.createCell(3).setCellValue("Gamma-Average");
		header.createCell(4).setCellValue("Gamma-Median");
		header.createCell(5).setCellValue("Alpha-Z-Average");
		header.createCell(6).setCellValue("Alpha-Z-Median");
		header.createCell(7).setCellValue("Gamma-Z-Average");
		header.createCell(8).setCellValue("Gamma-Z-Median");
		header.createCell(9).setCellValue("Alpha-IQR-Average");
		header.createCell(10).setCellValue("Alpha-IQR-Median");
		header.createCell(11).setCellValue("Gamma-IQR-Average");
		header.createCell(12).setCellValue("Gamma-IQR-Median");


		writeAverageAndMedian(sheet, ALPHA_POWERS, 0);
		writeAverageAndMedian(sheet, GAMMA_POWERS, 1);
		writeAverageAndMedian(sheet, ALPHA_ZSCORE, 2);
		writeAverageAndMedian(sheet, GAMMA_ZSCORE, 3);
		writeAverageAndMedian(sheet, ALPHA_IQR_SCORE, 4);
		writeAverageAndMedian(sheet, GAMMA_IQR_SCORE, 5);
		writeSeenVsUnseen();
		createCharts();
	}

	private void writeSeenVsUnseen() {
		LOG.debug("Writing Seen Vs Unseen");
		Sheet sheet = getSheet(DataSheet.SEEN_VS_UNSEEN);
		String alphaPowersLabel = ALPHA_POWERS.getSheetLabel();

		sheet.createRow(0).createCell(0).setCellValue("Power");
		writeAverageIf(sheet.createRow(1), 3, alphaPowersLabel);
		writeAverageIf(sheet.createRow(2), 1, alphaPowersLabel);
		writeAverageIf(sheet.createRow(3), 2, alphaPowersLabel);

		String iqrLabel = ALPHA_IQR_SCORE.getSheetLabel();
		sheet.createRow(4).createCell(0).setCellValue("IQR");
		writeAverageIf(sheet.createRow(5), 3, iqrLabel);
		writeAverageIf(sheet.createRow(6), 1, iqrLabel);
		writeAverageIf(sheet.createRow(7), 2, iqrLabel);

		String zScoreLabel = ALPHA_ZSCORE.getSheetLabel();
		sheet.createRow(8).createCell(0).setCellValue("Z-Score");
		writeAverageIf(sheet.createRow(9), 3, zScoreLabel);
		writeAverageIf(sheet.createRow(10), 1, zScoreLabel);
		writeAverageIf(sheet.createRow(11), 2, zScoreLabel);


	}

	private void writeAverageIf(Row row, int label, String sourceSheet) {
		Cell cell = row.createCell(0);
		cell.setCellValue(EpdUtil.getLabel(label));
		String formulaTemplate = "AVERAGEIF('%s'!B:B,\"=%d\",'%s'!%s:%s)";
		for (int i = 1; i <= 128; i++) {
			cell = row.createCell(i, CellType.FORMULA);
			String col = convertNumToColString(i + 1);
			String formula = String.format(formulaTemplate, sourceSheet, label, sourceSheet, col, col);
			cell.setCellFormula(formula);
		}
	}

	private void writeAverageAndMedian(Sheet destination, DataSheet source, int offset) {
		int physicalNumberOfRows = getSheet(source).getPhysicalNumberOfRows();
		for (int rowNb = 1; rowNb < physicalNumberOfRows; rowNb++) {
			Row row = getOrCreateRow(destination, rowNb);
			String sheetReference = "'" + source.getSheetLabel() + "'!C" + (rowNb + 1) + ":DZ" + (rowNb + 1);

			Cell cell = row.createCell(2 * offset + 1, CellType.FORMULA);
			String averageFormula = "AVERAGE(" + sheetReference + ")";
			cell.setCellFormula(averageFormula);

			cell = row.createCell(2 * offset + 2, CellType.FORMULA);
			String medianFormula = "MEDIAN(" + sheetReference + ")";
			cell.setCellFormula(medianFormula);
		}
	}

	private Row getOrCreateRow(Sheet destination, int rowNb) {
		Row row = destination.getRow(rowNb);
		if (row != null)
			return row;
		return destination.createRow(rowNb);
	}

	private void createCharts() {
		LOG.debug("Writing charts");
		Sheet alphaPowersSheet = getSheet(ALPHA_POWERS);
		int numberOfRows = alphaPowersSheet.getPhysicalNumberOfRows();
		ChartDataSource<Number> xAxisData = DataSources.fromNumericCellRange(alphaPowersSheet, new CellRangeAddress(1, numberOfRows, 1, 1));
		createFormulasCharts(numberOfRows, xAxisData);
		for (DataSheet dataSheet : DataSheet.values()) {
			if (dataSheet.isMatrix()) {
				createDataChart(dataSheet, xAxisData, numberOfRows);
			}
		}
	}

	private void createDataChart(DataSheet dataSheet, ChartDataSource<Number> xs, int numberOfRows) {
		Sheet sheet = getSheet(dataSheet);
		Drawing drawing = sheet.createDrawingPatriarch();
		ClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, 5, 5, 30, 30);
		Chart chart = drawing.createChart(anchor);
		chart.getOrCreateLegend().setPosition(LegendPosition.BOTTOM);
		LineChartData data = chart.getChartDataFactory().createLineChartData();

		for (int i = 1; i <= 128; i++) {
			ChartDataSource<Number> series = DataSources.fromNumericCellRange(sheet, new CellRangeAddress(1, numberOfRows, i + 1, i + 1));
			data.addSeries(xs, series).setTitle("Ch-" + i);
		}
		chart.plot(data, new ChartAxis[]{chart.getChartAxisFactory().createCategoryAxis(AxisPosition.BOTTOM), chart.getChartAxisFactory().createValueAxis(AxisPosition.LEFT)});
	}

	private void createFormulasCharts(int numberOfRows, ChartDataSource<Number> xAxisData) {
		Sheet formulasSheet = getSheet(FORMULAS);
		Drawing drawing = formulasSheet.createDrawingPatriarch();
		chartOffset = 5;
		createChart(formulasSheet, xAxisData, numberOfRows, drawing, 1, 2);
		chartOffset += 35;
		createChart(formulasSheet, xAxisData, numberOfRows, drawing, 3, 4);
		chartOffset += 35;
		createChart(formulasSheet, xAxisData, numberOfRows, drawing, 5, 7);
		chartOffset += 35;
		createChart(formulasSheet, xAxisData, numberOfRows, drawing, 9, 11);

		chartOffset += 35;
		createChart(formulasSheet, xAxisData, numberOfRows, drawing, 6, 8);

		chartOffset += 35;
		createChart(formulasSheet, xAxisData, numberOfRows, drawing, 10, 12);
	}


	private void createChart(Sheet sheet, ChartDataSource<Number> xs, int numberOfRows, Drawing drawing, int col1, int col2) {
		ClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, 5, chartOffset, 45, chartOffset + 25);
		Chart chart = drawing.createChart(anchor);
		chart.getOrCreateLegend().setPosition(LegendPosition.BOTTOM);
		LineChartData data = chart.getChartDataFactory().createLineChartData();

		ChartDataSource<Number> average = DataSources.fromNumericCellRange(sheet, new CellRangeAddress(1, numberOfRows, col1, col1));
		ChartDataSource<Number> median = DataSources.fromNumericCellRange(sheet, new CellRangeAddress(1, numberOfRows, col2, col2));

		data.addSeries(xs, average).setTitle(sheet.getRow(0).getCell(col1).getStringCellValue());
		data.addSeries(xs, median).setTitle(sheet.getRow(0).getCell(col2).getStringCellValue());
		chart.plot(data, new ChartAxis[]{chart.getChartAxisFactory().createCategoryAxis(AxisPosition.BOTTOM), chart.getChartAxisFactory().createValueAxis(AxisPosition.LEFT)});
	}

	public void writeHeaders() {
		for (DataSheet dataSheet : DataSheet.values()) {
			if (dataSheet.isMatrix()) {
				writeHeaders(getSheet(dataSheet));
			}
		}
	}

	private void writeHeaders(Sheet sheet) {
		Row row = sheet.createRow(0);
		Cell cell = row.createCell(0, CellType.STRING);
		cell.setCellValue("trial");
		cell = row.createCell(1, CellType.STRING);
		cell.setCellValue("result");
		for (int i = 1; i <= 128; i++) {
			cell = row.createCell(i + 1, CellType.STRING);
			cell.setCellValue("Ch-" + i);
		}
	}

	public void flush() {
		LOG.debug("Flushing Excel data");
		try (FileOutputStream out = new FileOutputStream(outputFile)) {
			workbook.write(out);
		} catch (IOException e) {
			LOG.error(e);
		}
	}

	@Override
	public void close() throws Exception {
		workbook.close();
	}
}
