package baseline.analysis.core.service.writting;

import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.Double.parseDouble;
import static org.apache.logging.log4j.LogManager.getLogger;

/**
 * Removes columns with noise and/or uncertain response from csv files
 */
public class CsvTrimmer {
	private static final List<Integer> NOISY_CHANNELS = Arrays.asList(6, 95, 35, 68);
	private static final Logger        LOG            = getLogger();

	private final File csvFile;
	private final File outputFolder;

	private List<String>       labels;
	private List<List<Double>> data;

	public CsvTrimmer(File csvFile, File outputFolder) {
		this.csvFile = csvFile;
		this.outputFolder = outputFolder;
	}


	public void readData() {
		data = new ArrayList<>();
		labels = new ArrayList<>();
		try (BufferedReader reader = new BufferedReader(new FileReader(csvFile))) {
			String line = reader.readLine();
			while (line != null) {
				List<Double> row = new ArrayList<>();
				data.add(row);
				String[] split = line.split(",");
				for (int j = 0; j < 128; j++) {
					row.add(parseDouble(split[j]));
				}
				labels.add(split[128]);
				line = reader.readLine();
			}
		} catch (IOException e) {
			LOG.error(e);
		}
	}

	public void filterColumns() {
		String fileName = csvFile.getName().replace(".csv", "-noZeros.csv");
		File outputFile = new File(outputFolder, fileName);
		try (PrintWriter writer = new PrintWriter(outputFile)) {
			for (int trialNumber = 0; trialNumber < data.size(); trialNumber++) {
				for (int i = 0; i < 128; i++) {
					if (NOISY_CHANNELS.contains(i)) {
						continue;
					}
					writer.write(getData(trialNumber, i) + ",");
				}
				writer.write(labels.get(trialNumber));
				writer.write("\n");
			}
			writer.flush();
		} catch (FileNotFoundException e) {
			LOG.error(e);
		}
	}

	private Object getData(int trialNumber, int column) {
		return data.get(trialNumber).get(column);
	}

	public void removeUncertain() {
		String fileName = csvFile.getName().replace(".csv", "-noUncertain.csv");
		File outputFile = new File(outputFolder, fileName);
		try (PrintWriter writer = new PrintWriter(outputFile)) {
			for (int trialNumber = 0; trialNumber < data.size(); trialNumber++) {
				if (labels.get(trialNumber).equalsIgnoreCase("uncertain")) {
					continue;
				}
				for (int i = 0; i < 128; i++) {
					writer.write(getData(trialNumber, i) + ",");
				}
				writer.write(labels.get(trialNumber));
				writer.write("\n");
			}
			writer.flush();
		} catch (FileNotFoundException e) {
			LOG.error(e);
		}
	}

	public void filterColumnsAndRemoveUncertain() {
		String fileName = csvFile.getName().replace(".csv", "-noUncertain-noZeros.csv");
		File outputFile = new File(outputFolder, fileName);
		try (PrintWriter writer = new PrintWriter(outputFile)) {
			for (int trialNumber = 0; trialNumber < data.size(); trialNumber++) {
				if (labels.get(trialNumber).equalsIgnoreCase("uncertain")) {
					continue;
				}
				for (int i = 0; i < 128; i++) {
					if (NOISY_CHANNELS.contains(i)) {
						continue;
					}
					writer.write(getData(trialNumber, i) + ",");
				}
				writer.write(labels.get(trialNumber));
				writer.write("\n");
			}
			writer.flush();
		} catch (FileNotFoundException e) {
			LOG.error(e);
		}
	}
}
