package baseline.analysis.core.service.writting;

import baseline.analysis.core.util.EpdUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

public class CsvWriter implements AutoCloseable {

	private final PrintWriter writer;

	public CsvWriter(File outputFile) throws FileNotFoundException {
		writer = new PrintWriter(outputFile);
	}

	public void writeData(double[][] data, List<Integer> labels) {
		for (int trialNumber = 0; trialNumber < 210; trialNumber++) {
			for (int i = 0; i < 128; i++) {
				writer.write(data[trialNumber][i] + ",");
			}
			writer.write(EpdUtil.getLabel(labels.get(trialNumber)));
			writer.write("\n");
		}
		writer.flush();
	}


	public void writeHeader() {
		String headerString = "trial" + "\t" + "result" + "\t";
		for (int i = 0 + 1; i < 129 + 1; i++) {
			headerString += ("Ch-" + i + "\t");
		}
		writer.write(headerString + "\n");
	}

	@Override
	public void close() {
		writer.flush();
		writer.close();
	}
}
