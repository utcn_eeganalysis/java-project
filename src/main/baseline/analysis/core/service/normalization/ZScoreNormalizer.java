package baseline.analysis.core.service.normalization;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

public class ZScoreNormalizer extends Normalizer {


	private final double mean;
	private final double standardDeviation;

	public ZScoreNormalizer(double[][] powers, DescriptiveStatistics statistics) {
		super(powers, statistics);
		mean = statistics.getMean();
		standardDeviation = statistics.getStandardDeviation();
	}

	@Override
	protected double getNormalizedValue(double val) {
		return (val - mean) / standardDeviation;
	}
}
