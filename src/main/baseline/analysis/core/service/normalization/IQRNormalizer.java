package baseline.analysis.core.service.normalization;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

public class IQRNormalizer extends Normalizer {


	private final double median;
	private final double q3;
	private final double q1;

	public IQRNormalizer(double[][] powers, DescriptiveStatistics statistics) {
		super(powers, statistics);
		median = statistics.getPercentile(50);
		q3 = statistics.getPercentile(75);
		q1 = statistics.getPercentile(25);
	}

	@Override
	protected double getNormalizedValue(double v) {
		return (v - median) / (q3 - q1);
	}
}
