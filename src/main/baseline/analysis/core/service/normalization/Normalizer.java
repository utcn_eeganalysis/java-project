package baseline.analysis.core.service.normalization;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

public abstract class Normalizer {

	private final   double[][]            powers;
	protected final DescriptiveStatistics statistics;


	public Normalizer(double[][] powers, DescriptiveStatistics statistics) {
		this.powers = powers;
		this.statistics = statistics;
	}

	public double[][] getNormalizedValues() {
		int nbRows = powers.length;
		int nbColumns = powers[0].length;
		double[][] result = new double[nbRows][nbColumns];
		for (int i = 0; i < nbRows; i++) {
			for (int j = 0; j < nbColumns; j++) {
				result[i][j] = getNormalizedValue(powers[i][j]);
			}
		}

		return result;
	}

	protected abstract double getNormalizedValue(double v);
}
