package baseline.analysis.core.service.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class ChannelBaselineData {

	private final int					channelNumber;

	private final List<Vector<Float>>	baselines	= new ArrayList<>();

	public ChannelBaselineData(int channelNumber) {
		this.channelNumber = channelNumber;
	}

	public int getChannelNumber() {
		return this.channelNumber;
	}

	public List<Vector<Float>> getBaselines() {
		return this.baselines;
	}

}
