package baseline.analysis.core.service.model;

import java.util.List;

public class EegDataset {

	private int							samplingFrequency;

	private int							numberOfSamples;

	private List<Event>					events;

	private List<ChannelBaselineData>	baselineData;

	private String						name;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSamplingFrequency() {
		return this.samplingFrequency;
	}

	public void setSamplingFrequency(int samplingFrequency) {
		this.samplingFrequency = samplingFrequency;
	}

	public int getNumberOfSamples() {
		return this.numberOfSamples;
	}

	public void setNumberOfSamples(int numberOfSamples) {
		this.numberOfSamples = numberOfSamples;
	}

	public List<Event> getEvents() {
		return this.events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public List<ChannelBaselineData> getBaselineData() {
		return this.baselineData;
	}

	public void setBaselineData(List<ChannelBaselineData> baselineData) {
		this.baselineData = baselineData;
	}
}
