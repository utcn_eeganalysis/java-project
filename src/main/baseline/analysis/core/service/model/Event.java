package baseline.analysis.core.service.model;

public class Event {

	private int	code;

	private int	timestamp;

	public Event(int eventCode, int eventTimestamp) {
		this.code = eventCode;
		this.timestamp = eventTimestamp;
	}

	public int getCode() {
		return this.code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public int getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp(int timestamp) {
		this.timestamp = timestamp;
	}

}
