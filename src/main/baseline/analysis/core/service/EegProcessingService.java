package baseline.analysis.core.service;

import baseline.analysis.core.service.fft.FastFourierTransformer;
import baseline.analysis.core.service.fft.FrequencyDomainChannelData;
import baseline.analysis.core.service.fft.FrequencyDomainDataset;
import baseline.analysis.core.service.model.EegDataset;
import baseline.analysis.core.service.normalization.IQRNormalizer;
import baseline.analysis.core.service.normalization.ZScoreNormalizer;
import baseline.analysis.core.service.reading.EegDataReader;
import baseline.analysis.core.service.writting.ExcelWriter;
import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static baseline.analysis.core.service.writting.ExcelWriter.DataSheet.*;
import static baseline.analysis.core.util.ComplexUtil.calculateAveragePower;
import static com.google.common.primitives.Doubles.concat;
import static java.util.stream.Collectors.toList;
import static org.apache.logging.log4j.LogManager.getLogger;

public class EegProcessingService {

	public static final int NB_CHANNELS = 128;
	public static final int NB_TRIALS   = 210;

	private final EegProperties properties = EegProperties.getInstance();
	private final Logger        LOG        = getLogger();
	private final String        epdFilePath;

	private EegDataset             eegDataSet;
	private FrequencyDomainDataset frequencyDomainDataset;

	private DescriptiveStatistics alphaStatistics;
	private DescriptiveStatistics gammaStatistics;
	private DescriptiveStatistics configuredStatistics;

	private double[][] alphaPowers      = null;
	private double[][] gammaPowers      = null;
	private double[][] configuredPowers = null;

	private double[][] alphaPhase       = null;
	private double[][] configuredPhase  = null;

	private double[][]    alphaZScore        = null;
	private double[][]    alphaIqrScore      = null;

	private double[][]    gammaZScore        = null;
	private double[][]    gammaIlqScore      = null;

	private double[][]    configuredIqrScore = null;
	private double[][]    configuredZScore   = null;

	private List<Integer> labels;

	public EegProcessingService(String epdFilePath) {
		this.epdFilePath = epdFilePath;
	}


	public void readDataAndApplyFFT() {
		try {
			int startEventCode = properties.getStartEventCode();
			int endEventCode = properties.getEndEventCode();
			EegDataReader dataReader = new EegDataReader(epdFilePath, startEventCode, endEventCode);
			LOG.debug("Reading Data...");
			eegDataSet = dataReader.readDataSet();
			LOG.debug("Applying FFT...");
			FastFourierTransformer transformer = new FastFourierTransformer(eegDataSet);
			transformer.applyTransformOnAllChannels();
			frequencyDomainDataset = transformer.getResult();
		} catch (IOException e) {
			LOG.error(e);
		}
	}

	public double[][] getAlphaPowers() {
		if (alphaPowers == null) {
			LOG.debug("Calculating average alpha powers...");
			alphaPowers = calculateAlphaPowers();
			alphaStatistics = new DescriptiveStatistics(concat(alphaPowers));
		}
		return alphaPowers;
	}

	public double[][] getGammaPowers() {
		if (gammaPowers == null) {
			LOG.debug("Calculating average gamma powers...");
			gammaPowers = calculateGammaPowers();
			gammaStatistics = new DescriptiveStatistics(concat(gammaPowers));
		}
		return gammaPowers;
	}

	public double[][] getConfiguredPowers() {
		if (configuredPowers == null) {
			LOG.debug("Calculating configured powers...");
			configuredPowers = calculateConfiguredPowers();
			configuredStatistics = new DescriptiveStatistics(concat(configuredPowers));
		}
		return configuredPowers;
	}

	public void exportToExcel(File file) {
		try (ExcelWriter writer = new ExcelWriter(file)) {
			writer.writeHeaders();
			exportToExcel(writer);
			writer.writeStatistics(alphaStatistics);
			writer.writeFormulas();
			writer.flush();
		} catch (Exception e) {
			LOG.error(e);
		}
	}

	public void exportToExcel(ExcelWriter writer) {
		List<Integer> labels = extractLabelsPerBaseline();
		String datasetName = eegDataSet.getName();
		LOG.debug("Calculating Alpha Power");
		writer.writeData(ALPHA_POWERS, getAlphaPowers(), labels, datasetName);
		LOG.debug("Calculating Alpha Phase");
		writer.writeData(ALPHA_PHASE, getAlphaPhase(), labels, datasetName);
		LOG.debug("Calculating Gamma Power");
		writer.writeData(GAMMA_POWERS, getGammaPowers(), labels, datasetName);
		LOG.debug("Calculating Alpha Z Score");
		writer.writeData(ALPHA_ZSCORE, getAlphaZScore(), labels, datasetName);
		LOG.debug("Calculating Alpha IQR Score");
		writer.writeData(ALPHA_IQR_SCORE, getAlphaIqrScore(), labels, datasetName);
		LOG.debug("Calculating Gamma Z Score");
		writer.writeData(GAMMA_ZSCORE, getGammaZScore(), labels, datasetName);
		LOG.debug("Calculating Gamma IQR Score");
		writer.writeData(GAMMA_IQR_SCORE, getGammaIlqScore(), labels, datasetName);

	}

	public double[][] getAlphaZScore() {
		if (alphaZScore == null) {
			alphaZScore = calculateZScore(getAlphaPowers(), alphaStatistics);
		}
		return alphaZScore;
	}

	public double[][] getAlphaIqrScore() {
		if (alphaIqrScore == null) {
			alphaIqrScore = calculateIlqScore(getAlphaPowers(), alphaStatistics);
		}
		return alphaIqrScore;
	}

	public double[][] getGammaZScore() {
		if (gammaZScore == null) {
			gammaZScore = calculateZScore(getGammaPowers(), gammaStatistics);
		}
		return gammaZScore;
	}

	public double[][] getGammaIlqScore() {
		if (gammaIlqScore == null) {
			gammaIlqScore = calculateIlqScore(getGammaPowers(), gammaStatistics);
		}
		return gammaIlqScore;
	}

	public double[][] getConfiguredIqrScore() {
		if (configuredIqrScore == null) {
			configuredIqrScore = calculateIlqScore(getConfiguredPowers(), configuredStatistics);
		}
		return configuredIqrScore;
	}

	public double[][] getConfiguredZScore() {
		if (configuredZScore == null) {
			configuredZScore = calculateZScore(getConfiguredPowers(), configuredStatistics);
		}
		return configuredZScore;
	}


	public List<Integer> getLabels() {
		if (labels == null) {
			labels = extractLabelsPerBaseline();
		}
		return labels;
	}

	public double[][] getAlphaPhase() {
		if (alphaPhase == null) {
			alphaPhase = calculateAlphaPhase();
		}
		return alphaPhase;
	}

	public double[][] getConfiguredPhase() {
		if (configuredPhase == null) {
			configuredPhase = calculateConfiguredPhase();
		}
		return configuredPhase;
	}

	private double[][] calculateIlqScore(double[][] powers, DescriptiveStatistics statistics) {
		return new IQRNormalizer(powers, statistics).getNormalizedValues();
	}

	private double[][] calculateZScore(double[][] powers, DescriptiveStatistics statistics) {
		return new ZScoreNormalizer(powers, statistics).getNormalizedValues();
	}

	/**
	 * Calculates the mean power per each trial. Will return a matrix of double[210][129].
	 */
	private double[][] calculateAlphaPowers() {
		double[][] result = new double[NB_TRIALS][NB_CHANNELS];
		for (int trialNumber = 0; trialNumber < NB_TRIALS; trialNumber++) {
			for (int channelNumber = 1; channelNumber <= NB_CHANNELS; channelNumber++) {
				FrequencyDomainChannelData channel = frequencyDomainDataset.get(channelNumber);
				double power = calculateAveragePower(channel.getAlphaBins(trialNumber));
				result[trialNumber][channelNumber - 1] = power;
			}
		}
		return result;
	}

	private double[][] calculateConfiguredPowers() {
		double[][] result = new double[NB_TRIALS][NB_CHANNELS];
		for (int trialNumber = 0; trialNumber < NB_TRIALS; trialNumber++) {
			for (int channelNumber = 1; channelNumber <= NB_CHANNELS; channelNumber++) {
				FrequencyDomainChannelData channel = frequencyDomainDataset.get(channelNumber);
				double power = calculateAveragePower(channel.getConfiguredPower(trialNumber));
				result[trialNumber][channelNumber - 1] = power;
			}
		}
		return result;
	}

	private double[][] calculateAlphaPhase() {
		double[][] result = new double[NB_TRIALS][NB_CHANNELS];
		for (int trialNumber = 0; trialNumber < NB_TRIALS; trialNumber++) {
			for (int channelNumber = 1; channelNumber <= NB_CHANNELS; channelNumber++) {
				FrequencyDomainChannelData channel = frequencyDomainDataset.get(channelNumber);
				List<Complex> alphaBins = channel.getAlphaBins(trialNumber);
				result[trialNumber][channelNumber - 1] = calculatePhase(alphaBins);
			}
		}
		return result;
	}

	private double[][] calculateConfiguredPhase() {
		double[][] result = new double[NB_TRIALS][NB_CHANNELS];
		for (int trialNumber = 0; trialNumber < NB_TRIALS; trialNumber++) {
			for (int channelNumber = 1; channelNumber <= NB_CHANNELS; channelNumber++) {
				FrequencyDomainChannelData channel = frequencyDomainDataset.get(channelNumber);
				List<Complex> bins = channel.getConfiguredPower(trialNumber);
				result[trialNumber][channelNumber - 1] = calculatePhase(bins);
			}
		}
		return result;
	}

	private double calculatePhase(List<Complex> alphaBins) {
		double phase = 0;
		for (Complex alphaBin : alphaBins) {
			phase += alphaBin.getArgument();
		}
		return phase / alphaBins.size();
	}

	private double[][] calculateGammaPowers() {
		double[][] result = new double[NB_TRIALS][NB_CHANNELS];
		for (int trialNumber = 0; trialNumber < NB_TRIALS; trialNumber++) {
			for (int channelNumber = 1; channelNumber <= NB_CHANNELS; channelNumber++) {
				FrequencyDomainChannelData channel = frequencyDomainDataset.get(channelNumber);
				double power = calculateAveragePower(channel.getGammaBins(trialNumber));
				result[trialNumber][channelNumber - 1] = power;
			}
		}
		return result;
	}

	private List<Integer> extractLabelsPerBaseline() {
		List<Integer> labels = Arrays.asList(1, 2, 3);
		return eegDataSet.getEvents().stream()
				.filter(e -> labels.contains(e.getCode()))
				.map(e -> e.getCode())
				.collect(toList());
	}
}
