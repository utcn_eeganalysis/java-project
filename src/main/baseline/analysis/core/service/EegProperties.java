package baseline.analysis.core.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import static baseline.analysis.core.util.ComplexUtil.*;
import static baseline.analysis.core.util.EpdUtil.IMAGE_APPEARS_CODE;
import static baseline.analysis.core.util.EpdUtil.RED_DOT_DISAPPEARS_CODE;
import static java.lang.Integer.parseInt;
import static java.lang.String.valueOf;

public class EegProperties {

	private final Properties prop;

	private static EegProperties instance;

	private EegProperties() {
		prop = new Properties();
		try (FileInputStream input = new FileInputStream("./config.properties")) {
			prop.load(input);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static EegProperties getInstance() {
		if (instance == null) instance = new EegProperties();
		return instance;
	}

	public int getStartEventCode() {
		return parseInt(prop.getProperty("startCode", valueOf(RED_DOT_DISAPPEARS_CODE)));
	}

	public int getEndEventCode() {
		return parseInt(prop.getProperty("endCode", valueOf(IMAGE_APPEARS_CODE)));
	}

	public int getStartFrequency() {
		return parseInt(prop.getProperty("startFrequency", valueOf(ALPHA_CHANNEL_LOWER_LIMIT)));
	}

	public int getEndFrequency() {
		return parseInt(prop.getProperty("endFrequency", valueOf(ALPHA_CHANNEL_UPPER_LIMIT)));
	}
}
