package baseline.analysis.core.util;

import org.apache.logging.log4j.Logger;

import java.util.List;

import static org.apache.logging.log4j.LogManager.getLogger;

public class ThreadUtil {

	private static final Logger LOG = getLogger();

	public static void waitForExecutionToFinish(List<Thread> tasks) {
		for (Thread thread : tasks) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				LOG.error(e);
			}
		}
	}
}
