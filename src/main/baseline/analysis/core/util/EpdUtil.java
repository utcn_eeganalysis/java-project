package baseline.analysis.core.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public final class EpdUtil {

	private EpdUtil() {
	}

	public static final int RED_DOT_APPEARS_CODE = 128;
	public static final int RED_DOT_DISAPPEARS_CODE = 150;
	public static final int IMAGE_APPEARS_CODE = 129;

	public static List<File> collectEpdFiles(final File rootFolder) {
		ArrayList<File> result = new ArrayList<>();
		for (final File fileEntry : rootFolder.listFiles()) {
			if (fileEntry.isDirectory()) {
				result.addAll(collectEpdFiles(fileEntry));
			} else {
				if (fileEntry.getName().endsWith(".epd")) {
					result.add(fileEntry);
				}
			}
		}
		return result;
	}

	public static String getLabel(Integer trialResult) {
		switch (trialResult) {
			case 1:
				return "seen";
			case 2:
				return "uncertain";
			case 3:
				return "unseen";
			default:
				return "INVALID CODE";
		}
	}
}
