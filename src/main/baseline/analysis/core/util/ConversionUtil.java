package baseline.analysis.core.util;

public final class ConversionUtil {

	/**
	 * Converts interger from Little Endian to Big Endian
	 *
	 * @param i the
	 *            little endian integer
	 * @return Big Endian Integer
	 */
	public static int littleEndianToBigEndian(int i) {
		return (i & 0xff) << 24 | (i & 0xff00) << 8 | (i & 0xff0000) >> 8 | (i >> 24) & 0xff;
	}

	private ConversionUtil() {
	}
}
