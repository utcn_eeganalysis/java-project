package baseline.analysis.core.util;

import baseline.analysis.core.service.EegProperties;
import org.apache.commons.math3.complex.Complex;

import java.util.List;

import static java.lang.Math.pow;

public final class ComplexUtil {

	public static final int ALPHA_CHANNEL_LOWER_LIMIT = 8;    // Hz
	public static final int ALPHA_CHANNEL_UPPER_LIMIT = 13;    // Hz

	public static final int GAMMA_CHANNEL_LOWER_LIMIT = 30;    // Hz
	public static final int GAMMA_CHANNEL_UPPER_LIMIT = 80;    // Hz


	public static double calculatePower(Complex complex) {
		return (pow(complex.getReal(), 2) + pow(complex.getReal(), 2)) / 2;
	}

	/*
	 * 0, deltaF, 2*deltaF,......maxFrequency
	 *
	 * 0, 1, 2,..................baselineLength
	 *
	 */
	public static int calculateAlphaStartIndex(int samplingFrequency, int baselineLength) {
		double maxFrequency = samplingFrequency / 2;
		double deltaFrequency = maxFrequency / baselineLength;
		return (int) (ALPHA_CHANNEL_LOWER_LIMIT / deltaFrequency);
	}

	public static int calculateAlphaEndIndex(int samplingFrequency, int baselineLength) {
		double maxFrequency = samplingFrequency / 2;
		double deltaFrequency = maxFrequency / baselineLength;
		return (int) (ALPHA_CHANNEL_UPPER_LIMIT / deltaFrequency) + 1; // subList does not include the last index
	}

	public static int calculateGammaStartIndex(int samplingFrequency, int baselineLength) {
		double maxFrequency = samplingFrequency / 2;
		double deltaFrequency = maxFrequency / baselineLength;
		return (int) (GAMMA_CHANNEL_LOWER_LIMIT / deltaFrequency);
	}

	public static int calculateGammaEndIndex(int samplingFrequency, int baselineLength) {
		double maxFrequency = samplingFrequency / 2;
		double deltaFrequency = maxFrequency / baselineLength;
		return (int) (GAMMA_CHANNEL_UPPER_LIMIT / deltaFrequency) + 1; // subList does not include the last index
	}

	public static double calculateAveragePower(List<Complex> powerBins) {
		double power = 0;
		for (Complex complex : powerBins) {
			power += calculatePower(complex);
		}
		return power / powerBins.size();
	}

	public static int calculateConfiguredFrequencyStartIndex(int samplingFrequency, int baselineLength) {
		double maxFrequency = samplingFrequency / 2;
		double deltaFrequency = maxFrequency / baselineLength;
		return (int) (EegProperties.getInstance().getStartFrequency() / deltaFrequency);
	}

	public static int calculateConfiguredFrequencyEndIndex(int samplingFrequency, int baselineLength) {
		double maxFrequency = samplingFrequency / 2;
		double deltaFrequency = maxFrequency / baselineLength;
		return (int) (EegProperties.getInstance().getEndFrequency() / deltaFrequency) + 1; // subList does not include the last index
	}


	private ComplexUtil() {
	}
}
