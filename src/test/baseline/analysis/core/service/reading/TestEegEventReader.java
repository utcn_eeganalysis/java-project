package baseline.analysis.core.service.reading;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import baseline.analysis.core.service.model.Event;

public class TestEegEventReader {

	private static final int	NUMBER_OF_EVENTS		= 1275;
	private static final int	TOTAL_NUMBER_OF_SAMPLES	= 2528256;
	private static final String	DATASET_NAME			= "\\Dots_30_001";
	private static final String	FOLDER_PATH				= "C:\\Users\\buzeavlad\\Desktop\\EEG\\Data+EEG Processor" + DATASET_NAME;
	private static final String	TIMESTAMPS_FILE_PATH	= FOLDER_PATH + DATASET_NAME + "-Event-Timestamps.bin";
	private static final String	CODES_FILE_PATH			= FOLDER_PATH + DATASET_NAME + "-Event-Codes.bin";

	@Test
	@Ignore
	public void testReadEventData() throws IOException {
		EegEventReader reader = new EegEventReader(new File(CODES_FILE_PATH), new File(TIMESTAMPS_FILE_PATH), NUMBER_OF_EVENTS);
		List<Event> data = reader.readEventData();
		Event previousPair = data.get(0);
		for (Event pair : data) {
			Integer timestamp = pair.getTimestamp();
			assertTrue(timestamp > 0);
			assertTrue(timestamp >= previousPair.getTimestamp());
			assertTrue(timestamp < TOTAL_NUMBER_OF_SAMPLES);
			assertTrue(pair.getCode() > 0);
			assertTrue(pair.getCode() < 256);
			if (pair.getCode() == 129) {
				assertEquals(150, previousPair.getCode());
			}
			System.out.println(pair.getCode() + "\t-\t" + timestamp);
			previousPair = pair;
		}
		assertTrue(previousPair.getTimestamp() < TOTAL_NUMBER_OF_SAMPLES);
		long nbBaselines = data.stream().filter(elem -> elem.getCode() == 150).count();
		assertEquals(210, nbBaselines);

	}

}
