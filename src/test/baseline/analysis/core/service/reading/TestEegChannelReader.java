package baseline.analysis.core.service.reading;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;

import static java.lang.String.format;

public class TestEegChannelReader {

	private static final int	NUMBER_OF_SAMPLES	= 1595392;
	private static final String	CHANNEL_FILE_PATH	= "src/test/resources/Dots_30_002-Ch001.bin";

	@Test
	public void testReadChannelData() throws IOException {
		baseline.analysis.core.service.reading.EegChannelReader reader = new EegChannelReader(new File(CHANNEL_FILE_PATH), NUMBER_OF_SAMPLES);
		Vector<Float> channelData = reader.readDataBetween(0, 512);
		PrintWriter writer = new PrintWriter("samples.txt");
		for (Float float1 : channelData) {
			String string = format("%f", float1);
			System.out.println(string);
			writer.println(string);
		}
		writer.close();
	}

}
