package baseline.analysis.core.service.reading;

import baseline.analysis.core.service.EegProperties;
import org.junit.Test;

public class TestEegProperties {

	@Test
	public void testProps() {
		EegProperties instance = EegProperties.getInstance();
		System.out.println(instance.getStartEventCode());
		System.out.println(instance.getEndEventCode());
		System.out.println(instance.getStartFrequency());
		System.out.println(instance.getEndFrequency());
	}
}
