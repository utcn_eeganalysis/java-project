package baseline.analysis.core.service.reading;

import baseline.analysis.core.service.fft.FastFourierTransformer;
import baseline.analysis.core.service.model.EegDataset;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class TestEegDataReader {

	private static final String EPD_FILE_PATH = "E:\\EEG\\Data+EEG Processor\\Dots_30_002\\Dots_30_002.epd";

	@Test
	@Ignore
	public void testReadExperimentData() throws IOException {
		EegDataReader reader = new EegDataReader(EPD_FILE_PATH, 150, 129);
		EegDataset dataSet = reader.readDataSet();
		assertEquals(1024, dataSet.getSamplingFrequency());
		assertEquals(1595392, dataSet.getNumberOfSamples());
		assertEquals(1275, dataSet.getEvents().size());
		assertEquals(129, dataSet.getBaselineData().size());
		FastFourierTransformer transformer = new FastFourierTransformer(dataSet);
		transformer.applyTransformOnAllChannels();
	}
}
